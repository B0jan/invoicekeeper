//
//  UIButtonExtension.swift
//  invoiceKeeper
//
//  Created by Administrator on 16/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UIButton
extension UIButton{
    
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        color.setFill()
        UIRectFill(rect)
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIView.animate(withDuration: 1) {
            self.setBackgroundImage(colorImage, for: state)
        }
        
    }
    
}
