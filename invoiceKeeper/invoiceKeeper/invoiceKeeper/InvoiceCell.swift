//
//  InvoiceCell.swift
//  invoiceKeeper
//
//  Created by Administrator on 16/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//

import Foundation
import UIKit
class FriendTVCell: UITableViewCell {
    
    let shadowLayer = CALayer()
    
    
    var invoiceImage:UIImageView = {
        var imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.backgroundColor = .white
        imgView.isOpaque = true
        imgView.layer.isOpaque = true
        imgView.clipsToBounds = true
        return imgView
    }()
    
    var invoiceTitle:UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = UIColor.darkGray
        lbl.backgroundColor = .white
        lbl.numberOfLines = 1
        return lbl
    }()
    
    var invoiceDesc:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.darkGray
        lbl.backgroundColor = .white
        lbl.numberOfLines = 1
        lbl.text = "Status"
        return lbl
    }()
    
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //    MARK: - Inits and configure
    override func draw(_ rect: CGRect) {
  
        invoiceImage.layer.cornerRadius = invoiceImage.frame.width/2
        invoiceImage.layer.borderColor = UIColor.black.cgColor
        invoiceImage.layer.borderWidth = 1
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(invoiceImage)
        self.addSubview(invoiceTitle)
        self.addSubview(invoiceDesc)
        
        

        
    }
    
 
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configure(with user:Invoice){
     
    }
    
}
