//
//  Invoice.swift
//  invoiceKeeper
//
//  Created by Administrator on 14/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//

import Foundation

struct Invoice {
    var image:Data
    var title:String
    var expirationDate:Date
    var invoiceDescription:String
    
    
    init(image:Data,expirationDate:Date,invoiceDescription:String,title:String) {
        self.image = image
        self.expirationDate = expirationDate
        self.invoiceDescription = invoiceDescription
        self.title = title
    }
}


