//
//  NewInvoiceViewController.swift
//  invoiceKeeper
//
//  Created by Administrator on 14/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//

import UIKit


class NewInvoiceViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    
    
    
    //    MARK: - UIElements
  
    
    var scrollView:UIScrollView = {
        let scView = UIScrollView()
        scView.translatesAutoresizingMaskIntoConstraints = false
        scView.backgroundColor = .blue
        
        return scView
    }()
    
    var scrollFixer:UIView = {
        var sf = UIView()
        sf.translatesAutoresizingMaskIntoConstraints = false
        sf.backgroundColor = .red
        return sf
    }()
    
    
    var imageView:UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .yellow
        
        return imageView
    }()
    
    
    var captureNewInvoice:UIButton = {
          var btn = UIButton()
          btn.translatesAutoresizingMaskIntoConstraints = false
          btn.backgroundColor = .black
          
          return btn
      }()
    
    var dateTextField:UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .brown
        return textField
    }()
    
    var descriptionView:UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.backgroundColor = .cyan
        
        
       return textView
    }()
    
      
    //    MARK: - Properties
    var imagePicker: UIImagePickerController!
    let datePicker = UIDatePicker()
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        showDatePicker()
        // Do any additional setup after loading the view.
    }
    
    
    
    //    MARK: - Actions
    @objc func addNewImagePressed(){
        if SPPermission.isAllowed(.camera){
            imagePicker =  UIImagePickerController()
                   imagePicker.delegate = self
                   imagePicker.sourceType = .camera
                   present(imagePicker, animated: true, completion: nil)
            
        } else {
            SPPermission.request(.camera) {
                if SPPermission.isAllowed(.camera){
                    
                }
            }
        }
        
        
    }



    //    MARK: - Protocol functions
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
             imagePicker.dismiss(animated: true, completion: nil)
        imageView.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
    }
    
    //    MARK: - Helpers
    func setupUI(){
        let scrolHeight:NSLayoutConstraint = scrollView.heightAnchor.constraint(equalTo: view.heightAnchor, constant: 0)
        scrolHeight.priority = UILayoutPriority(500)
        
        view.addSubview(scrollView)
        view.addSubview(scrollFixer)
        scrollView.addSubview(imageView)
        scrollView.addSubview(captureNewInvoice)
        scrollView.addSubview(dateTextField)
        scrollView.addSubview(descriptionView)
        
        captureNewInvoice.addTarget(self, action: #selector(addNewImagePressed), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant:0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrolHeight,
            
            scrollFixer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            scrollFixer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            scrollFixer.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            scrollFixer.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0),
            scrollFixer.heightAnchor.constraint(equalToConstant: 1),
            
            
            imageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            imageView.heightAnchor.constraint(equalToConstant: 300),
            
            
            
            captureNewInvoice.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            captureNewInvoice.heightAnchor.constraint(equalToConstant: 50),
            captureNewInvoice.widthAnchor.constraint(equalToConstant: 50),
            captureNewInvoice.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -15),
            
            
            dateTextField.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10),
                       dateTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
                       dateTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
                       dateTextField.heightAnchor.constraint(equalToConstant: 60),
                      
                       
            
            descriptionView.topAnchor.constraint(equalTo: dateTextField.bottomAnchor, constant: 10),
                               descriptionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
                               descriptionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
                               descriptionView.heightAnchor.constraint(equalToConstant: 100),
                               descriptionView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0)
                               
            
            
        ])
    }
    
    
    func showDatePicker(){
       //Formate Date
       datePicker.datePickerMode = .date

      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

     dateTextField.inputAccessoryView = toolbar
     dateTextField.inputView = datePicker

    }

     @objc func donedatePicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "dd/MM/yyyy"
      dateTextField.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
       self.view.endEditing(true)
     }
    
}
