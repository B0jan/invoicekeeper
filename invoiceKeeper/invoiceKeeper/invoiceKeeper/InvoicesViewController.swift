//
//  InvoicesViewController.swift
//  invoiceKeeper
//
//  Created by Administrator on 16/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//

import UIKit

class InvoicesViewController: UIViewController {
    
    
    
    
    //    MARK: - UIElements
    lazy var customSearchView = CustomSearchView()
    
    var allSubViews = [UIView]()
    
    var tableView:UITableView = {
        var tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .ivBlueMedium
        return tv
    }()
    
    lazy var dismissBUtton:UIButton = {
        let b = UIButton()
        //Icons made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
        b.setImage(UIImage(named: "closeIKT"), for: .normal)
        b.tintColor = .white
        b.translatesAutoresizingMaskIntoConstraints = false
        b.addTarget(self, action: #selector(dismissMe), for: .touchUpInside)
        b.imageView?.contentMode = .scaleAspectFit
        return b
    }()
    
    //    MARK: - Properties
    lazy var widthCenter = UIScreen.main.bounds.width/2
    lazy var heightCenter = UIScreen.main.bounds.height/2
    
    //    MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        view.subviews.forEach { (v) in
            allSubViews.append(v)
        }
        // Do any additional setup after loading the view.
    }
    
    //    MARK: - Actions
    @objc func dismissMe(){
        view.backgroundColor = .ivBlueMedium
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            for v in self.allSubViews {
                let (x, y) = self.setXandY(v)
                let r1 = CGAffineTransform(scaleX: 0.01, y: 0.01)
                    .concatenating(CGAffineTransform(translationX: x, y: y))
                v.transform = r1
                v.isHidden = false
            }
        } completion: { (done) in
            if done {self.dismiss(animated: false, completion: nil)}
        }

        
    }
    //    MARK: - Protocol functions
    
    
    //    MARK: - Helpers
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for v in self.allSubViews {
            let (x, y) = setXandY(v)
            let r1 = CGAffineTransform(scaleX: 0.01, y: 0.01)
                .concatenating(CGAffineTransform(translationX: x, y: y))
            v.transform = r1
            v.isHidden = false
        }
        UIView.animate(withDuration: 0.3) {
            for v in self.allSubViews {
                v.transform = CGAffineTransform.identity
            }
            self.view.backgroundColor = .ivBlueDark
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated)
        for v in self.allSubViews {
            v.isHidden = true
        }
    }
    
    private func setupUI(){

        view.addSubview(customSearchView)
        view.addSubview(tableView)
        view.addSubview(dismissBUtton)
        customSearchView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            
            customSearchView.topAnchor.constraint(equalTo: view.topAnchor,constant: 12),
            customSearchView.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 0),
            customSearchView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -50),
            customSearchView.heightAnchor.constraint(equalToConstant: 50),
            
            dismissBUtton.leadingAnchor.constraint(equalTo: customSearchView.trailingAnchor, constant: 8),
            dismissBUtton.centerYAnchor.constraint(equalTo: customSearchView.centerYAnchor,constant: 0),
            dismissBUtton.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -8),
            dismissBUtton.heightAnchor.constraint(equalToConstant: 40),
            
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            tableView.topAnchor.constraint(equalTo: customSearchView.bottomAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
            
            
        ])
        self.view.backgroundColor = .ivBlueMedium
        
        //        DispatchQueue.main.async {
        //            self.view.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        //
        //        }
        
        
    }
    
    private func setXandY(_ v:UIView)->(x:CGFloat, y: CGFloat){
        let x = widthCenter - v.frame.midX
        let y = heightCenter - v.frame.midY
        return (x, y)
    }
    
}
