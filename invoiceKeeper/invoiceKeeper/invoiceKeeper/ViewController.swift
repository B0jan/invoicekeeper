//
//  ViewController.swift
//  invoiceKeeper
//
//  Created by Ladlam on 12.09.20.
//  Copyright © 2020 imd. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //    MARK: - UIElements
    lazy var addFromGallery:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .ivBlueMedium
        btn.setTitle("Add from gallery", for: .normal)
        btn.layer.borderWidth = 2
        btn.layer.borderColor = UIColor.ivBlueLight.cgColor
        btn.layer.cornerRadius = 15
        btn.addTarget(self, action: #selector(newInvoicePressed), for: .touchUpInside)

        return btn
    }()
    
    lazy var addFromCamera:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .ivBlueMedium
        btn.setTitle("Add from camera", for: .normal)
        btn.layer.borderWidth = 2
        btn.layer.borderColor = UIColor.ivBlueLight.cgColor
        btn.layer.cornerRadius = 15
        btn.addTarget(self, action: #selector(newInvoicePressed), for: .touchUpInside)
        return btn
    }()
    
    lazy var showAllInvoices:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .ivBlueMedium
        btn.setTitle("Show all", for: .normal)
        btn.layer.borderWidth = 2
        btn.layer.borderColor = UIColor.ivBlueLight.cgColor
        btn.layer.cornerRadius = 15
        btn.addTarget(self, action: #selector(showAllPressed), for: .touchUpInside)

        return btn
    }()
    
    
    lazy var tableView:UITableView = {
        var tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.backgroundColor = .ivBlueMedium
        tv.delegate = self
        tv.dataSource = self
        tv.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tv.layer.borderWidth = 2
        tv.layer.borderColor = UIColor.ivBlueLight.cgColor
        tv.layer.cornerRadius = 4
        tv.tableFooterView = UIView()
        return tv
    }()
    
    lazy var noRecentsLabel:UILabel = {
        let l = UILabel()
        l.text = "Nothing saved yet 🧐"
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .white
        l.numberOfLines = 0
        l.textAlignment = .center
        l.font = UIFont.systemFont(ofSize: 40)
        return l
    }()
    
    //    MARK: - Properties
    
    lazy var widthCenter = UIScreen.main.bounds.width/2
    lazy var heightCenter = UIScreen.main.bounds.height/2
    
    lazy var portraitConstraints = [
        addFromGallery.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 0),
        addFromGallery.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
        addFromGallery.widthAnchor.constraint(equalTo: tableView.widthAnchor, multiplier: 0.48),
        addFromGallery.heightAnchor.constraint(equalToConstant: 40),
        
        addFromCamera.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: 0),
        addFromCamera.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
        addFromCamera.widthAnchor.constraint(equalTo: tableView.widthAnchor, multiplier: 0.48),
        addFromCamera.heightAnchor.constraint(equalToConstant: 40),
        
        tableView.topAnchor.constraint(equalTo: addFromCamera.bottomAnchor, constant: 30),
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100),
        
        showAllInvoices.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 20),
        showAllInvoices.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
        showAllInvoices.widthAnchor.constraint(equalToConstant: 120),
        showAllInvoices.heightAnchor.constraint(equalToConstant: 40),
        
        noRecentsLabel.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
        noRecentsLabel.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 8),
        noRecentsLabel.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: -8)
    ]
    
    lazy var landscapeConstraints = [
        addFromGallery.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 0),
        addFromGallery.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
        addFromGallery.widthAnchor.constraint(equalTo: tableView.widthAnchor, multiplier: 0.48),
        addFromGallery.heightAnchor.constraint(equalToConstant: 45),
        
        addFromCamera.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: 0),
        addFromCamera.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
        addFromCamera.widthAnchor.constraint(equalTo: tableView.widthAnchor, multiplier: 0.48),
        addFromCamera.heightAnchor.constraint(equalToConstant: 45),
        
        tableView.topAnchor.constraint(equalTo: addFromCamera.bottomAnchor, constant: 15),
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60),
        
        showAllInvoices.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 10),
        showAllInvoices.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
        showAllInvoices.widthAnchor.constraint(equalToConstant: 120),
        showAllInvoices.heightAnchor.constraint(equalToConstant: 40),
        
        noRecentsLabel.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
        noRecentsLabel.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 8),
        noRecentsLabel.trailingAnchor.constraint(equalTo: tableView.trailingAnchor, constant: -8)
    ]
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            for v in self.view.subviews {
                v.transform = CGAffineTransform.identity
            }
        }
    }
    
    
    //    MARK: - Actions
    
    @objc func newInvoicePressed(_ sender:UIButton){
        if sender == addFromGallery{
            let vc = NewInvoiceViewController()
            self.present(vc, animated: true, completion: nil)
        } else {
            let vc = NewScanViewController()
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @objc func showAllPressed(){
        let vc = InvoicesViewController()
        UIView.animate(withDuration: 0.3) {
            for v in self.view.subviews {
                let (x, y) = self.setXandY(v)
                let r1 = CGAffineTransform(scaleX: 0.01, y: 0.01)
                    .concatenating(CGAffineTransform(translationX: x, y: y))
                v.transform = r1
            }
            
        }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
        
    }
    //    MARK: - Protocol functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        return cell!
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            NSLayoutConstraint.deactivate(portraitConstraints)
            NSLayoutConstraint.activate(landscapeConstraints)
        } else {
            print("Portrait")
            NSLayoutConstraint.deactivate(landscapeConstraints)
            NSLayoutConstraint.activate(portraitConstraints)
        }
    }
    
    
    //    MARK: - Helpers
    private func setupUI(){
        view.addSubview(addFromGallery)
        view.addSubview(addFromCamera)
        view.addSubview(tableView)
        view.addSubview(showAllInvoices)
        view.addSubview(noRecentsLabel)
        
        NSLayoutConstraint.activate(portraitConstraints)
        
        view.backgroundColor = .ivBlueMedium
        
    }
    
    private func setXandY(_ v:UIView)->(x:CGFloat, y: CGFloat){
        let x = widthCenter - v.frame.midX
        let y = heightCenter - v.frame.midY
        return (x,y)
    }
    
}

