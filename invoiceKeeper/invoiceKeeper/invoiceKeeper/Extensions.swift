//
//  Extensions.swift
//  invoiceKeeper
//
//  Created by Ladlam on 20.09.20.
//  Copyright © 2020 imd. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static private func to255(r:CGFloat, g: CGFloat, b:CGFloat)->UIColor{
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static var ivBlueMedium:UIColor {
        return UIColor.to255(r: 78, g: 165, b: 210)
    }
    
    static var ivBlueLight: UIColor{
        return UIColor.to255(r: 66, g: 294, b: 201)
    }
    
    static var ivBlueDark:UIColor {
        return UIColor.to255(r: 73, g:117, b: 170)
    }
}

