//
//  CustomSearchView.swift
//  invoiceKeeper
//
//  Created by Administrator on 16/09/2020.
//  Copyright © 2020 imd. All rights reserved.
//



import UIKit

class CustomSearchView: UIView, UITextFieldDelegate {
    
    //MARK:-Properties
    var dismissKeyboard:((Bool)->())?
    var searchHeight:NSLayoutConstraint!{
        didSet{
            filterSortHeight.constant = searchHeight.constant
            self.layoutIfNeeded()
        }
    }
    var filterSortHeight:NSLayoutConstraint!
    
    //    MARK: - UIElements
    let searchField:UITextField = {
       let fld = UITextField()
//        fld.borderStyle = .roundedRect
        fld.backgroundColor = .white
        fld.layer.cornerRadius = 15.0
        fld.layer.borderWidth = 1
        fld.layer.borderColor = UIColor.gray.cgColor
        fld.placeholder = " Search invoices..."
        fld.font = UIFont.systemFont(ofSize: 17)
        fld.clearButtonMode = .whileEditing
        return fld
    }()
    
    let filterSortBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("F", for: .normal)
        btn.setTitleColor(UIColor.gray, for: .normal)
        btn.setTitleColor(UIColor.white, for: .highlighted)
//        btn.isHidden = true
      //  btn.sizeToFit()
    //    btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
        btn.clipsToBounds = true
        btn.setBackgroundColor(.blue, for: .highlighted)
        btn.setBackgroundColor(.white, for: .normal)
//        btn.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
        
        return btn
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        filterSortBtn.layer.cornerRadius = 20
    }
    
//    lazy var searchStack:UIStackView = {
//        let ps = UIStackView()
//        ps.axis = .horizontal
//        ps.alignment = UIStackView.Alignment.fill
//        ps.distribution = .fillProportionally
//        ps.addArrangedSubview(filterSortBtn)
//        ps.addArrangedSubview(searchField)
//        ps.spacing = 4
////        filterSortBtn.frame.size = CGSize(width: filterSortBtn.intrinsicContentSize.width+45, height: 50)
////        ps.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        return ps
//    }()

    //    MARK:- Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .ivBlueDark
//        searchHeight = searchStack.heightAnchor.constraint(equalToConstant: 40)
//        DispatchQueue.main.async {
//            self.addSubview(self.searchStack)
//            self.searchStack.frame = CGRect(x: 8, y: 8, width: Manager.shared.screenWidth - 16, height: 50)
//        }
        searchField.delegate = self
        searchField.translatesAutoresizingMaskIntoConstraints = false
        filterSortBtn.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(searchField)
        self.addSubview(filterSortBtn)
        
        
        searchHeight = searchField.heightAnchor.constraint(equalToConstant: 40)
        filterSortHeight = filterSortBtn.heightAnchor.constraint(equalToConstant: 40)
        NSLayoutConstraint.activate([

            filterSortBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            filterSortBtn.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
//            filterSortBtn.heightAnchor.constraint(equalToConstant: 50),
            filterSortHeight,
            filterSortBtn.widthAnchor.constraint(equalToConstant: 50),

            searchField.leadingAnchor.constraint(equalTo: filterSortBtn.trailingAnchor, constant: 4),
            searchField.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
//            searchField.heightAnchor.constraint(equalToConstant: 50),
            searchHeight,
            searchField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),


        ])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //    MARK:- Actions
    @objc func cancelTapped(_ sender:UIButton){
//        searchField.eClearText()
//        dismissKeyboard?(true)
        sender.setBackgroundColor(.red, for: .highlighted)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
